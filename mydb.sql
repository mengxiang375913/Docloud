-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: docloud
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acls_accesscontrollist`
--

DROP TABLE IF EXISTS `acls_accesscontrollist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acls_accesscontrollist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(10) unsigned NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acls_accesscontrollist_content_type_id_071de930_uniq` (`content_type_id`,`object_id`,`role_id`),
  KEY `acls_accesscontrollist_role_id_935b00e2_fk_permissions_role_id` (`role_id`),
  CONSTRAINT `acls_accessco_content_type_id_324a36ca_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `acls_accesscontrollist_role_id_935b00e2_fk_permissions_role_id` FOREIGN KEY (`role_id`) REFERENCES `permissions_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acls_accesscontrollist`
--

LOCK TABLES `acls_accesscontrollist` WRITE;
/*!40000 ALTER TABLE `acls_accesscontrollist` DISABLE KEYS */;
/*!40000 ALTER TABLE `acls_accesscontrollist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acls_accesscontrollist_permissions`
--

DROP TABLE IF EXISTS `acls_accesscontrollist_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acls_accesscontrollist_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accesscontrollist_id` int(11) NOT NULL,
  `storedpermission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `acls_accesscontrollist_permis_accesscontrollist_id_a6be00ef_uniq` (`accesscontrollist_id`,`storedpermission_id`),
  KEY `storedpermission_id_342d4fc3_fk_permissions_storedpermission_id` (`storedpermission_id`),
  CONSTRAINT `acls__accesscontrollist_id_c84800e5_fk_acls_accesscontrollist_id` FOREIGN KEY (`accesscontrollist_id`) REFERENCES `acls_accesscontrollist` (`id`),
  CONSTRAINT `storedpermission_id_342d4fc3_fk_permissions_storedpermission_id` FOREIGN KEY (`storedpermission_id`) REFERENCES `permissions_storedpermission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acls_accesscontrollist_permissions`
--

LOCK TABLES `acls_accesscontrollist_permissions` WRITE;
/*!40000 ALTER TABLE `acls_accesscontrollist_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `acls_accesscontrollist_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actstream_action`
--

DROP TABLE IF EXISTS `actstream_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actstream_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_object_id` varchar(255) NOT NULL,
  `verb` varchar(255) NOT NULL,
  `description` longtext,
  `target_object_id` varchar(255) DEFAULT NULL,
  `action_object_object_id` varchar(255) DEFAULT NULL,
  `timestamp` datetime(6) NOT NULL,
  `public` tinyint(1) NOT NULL,
  `action_object_content_type_id` int(11) DEFAULT NULL,
  `actor_content_type_id` int(11) NOT NULL,
  `target_content_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `D21652a41086e7e3faab058c28e76e68` (`action_object_content_type_id`),
  KEY `actstre_actor_content_type_id_d5e5ec2a_fk_django_content_type_id` (`actor_content_type_id`),
  KEY `actstr_target_content_type_id_187fa164_fk_django_content_type_id` (`target_content_type_id`),
  KEY `actstream_action_c4f7c191` (`actor_object_id`),
  KEY `actstream_action_b512ddf1` (`verb`),
  KEY `actstream_action_1cd2a6ae` (`target_object_id`),
  KEY `actstream_action_9063443c` (`action_object_object_id`),
  KEY `actstream_action_d7e6d55b` (`timestamp`),
  KEY `actstream_action_4c9184f3` (`public`),
  CONSTRAINT `D21652a41086e7e3faab058c28e76e68` FOREIGN KEY (`action_object_content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `actstr_target_content_type_id_187fa164_fk_django_content_type_id` FOREIGN KEY (`target_content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `actstre_actor_content_type_id_d5e5ec2a_fk_django_content_type_id` FOREIGN KEY (`actor_content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actstream_action`
--

LOCK TABLES `actstream_action` WRITE;
/*!40000 ALTER TABLE `actstream_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `actstream_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actstream_follow`
--

DROP TABLE IF EXISTS `actstream_follow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actstream_follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` varchar(255) NOT NULL,
  `actor_only` tinyint(1) NOT NULL,
  `started` datetime(6) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `actstream_follow_user_id_63ca7c27_uniq` (`user_id`,`content_type_id`,`object_id`),
  KEY `actstream_fol_content_type_id_ba287eb9_fk_django_content_type_id` (`content_type_id`),
  KEY `actstream_follow_af31437c` (`object_id`),
  KEY `actstream_follow_3bebb2f8` (`started`),
  CONSTRAINT `actstream_fol_content_type_id_ba287eb9_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `actstream_follow_user_id_e9d4e1ff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actstream_follow`
--

LOCK TABLES `actstream_follow` WRITE;
/*!40000 ALTER TABLE `actstream_follow` DISABLE KEYS */;
/*!40000 ALTER TABLE `actstream_follow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add permission',3,'add_permission'),(8,'Can change permission',3,'change_permission'),(9,'Can delete permission',3,'delete_permission'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add action',8,'add_action'),(23,'Can change action',8,'change_action'),(24,'Can delete action',8,'delete_action'),(25,'Can add follow',9,'add_follow'),(26,'Can change follow',9,'change_follow'),(27,'Can delete follow',9,'delete_follow'),(28,'Can add Autoadmin properties',10,'add_autoadminsingleton'),(29,'Can change Autoadmin properties',10,'change_autoadminsingleton'),(30,'Can delete Autoadmin properties',10,'delete_autoadminsingleton'),(31,'Can add cors model',11,'add_corsmodel'),(32,'Can change cors model',11,'change_corsmodel'),(33,'Can delete cors model',11,'delete_corsmodel'),(34,'Can add periodic task',12,'add_periodictask'),(35,'Can change periodic task',12,'change_periodictask'),(36,'Can delete periodic task',12,'delete_periodictask'),(37,'Can add crontab',13,'add_crontabschedule'),(38,'Can change crontab',13,'change_crontabschedule'),(39,'Can delete crontab',13,'delete_crontabschedule'),(40,'Can add interval',14,'add_intervalschedule'),(41,'Can change interval',14,'change_intervalschedule'),(42,'Can delete interval',14,'delete_intervalschedule'),(43,'Can add periodic tasks',15,'add_periodictasks'),(44,'Can change periodic tasks',15,'change_periodictasks'),(45,'Can delete periodic tasks',15,'delete_periodictasks'),(46,'Can add task state',16,'add_taskmeta'),(47,'Can change task state',16,'change_taskmeta'),(48,'Can delete task state',16,'delete_taskmeta'),(49,'Can add saved group result',17,'add_tasksetmeta'),(50,'Can change saved group result',17,'change_tasksetmeta'),(51,'Can delete saved group result',17,'delete_tasksetmeta'),(52,'Can add worker',18,'add_workerstate'),(53,'Can change worker',18,'change_workerstate'),(54,'Can delete worker',18,'delete_workerstate'),(55,'Can add task',19,'add_taskstate'),(56,'Can change task',19,'change_taskstate'),(57,'Can delete task',19,'delete_taskstate'),(58,'Can add token',20,'add_token'),(59,'Can change token',20,'change_token'),(60,'Can delete token',20,'delete_token'),(61,'Can add Access entry',21,'add_accesscontrollist'),(62,'Can change Access entry',21,'change_accesscontrollist'),(63,'Can delete Access entry',21,'delete_accesscontrollist'),(64,'Can add Shared uploaded file',22,'add_shareduploadedfile'),(65,'Can change Shared uploaded file',22,'change_shareduploadedfile'),(66,'Can delete Shared uploaded file',22,'delete_shareduploadedfile'),(67,'Can add User locale profile',23,'add_userlocaleprofile'),(68,'Can change User locale profile',23,'change_userlocaleprofile'),(69,'Can delete User locale profile',23,'delete_userlocaleprofile'),(70,'Can add Error log entry',24,'add_errorlogentry'),(71,'Can change Error log entry',24,'change_errorlogentry'),(72,'Can delete Error log entry',24,'delete_errorlogentry'),(73,'Can add Transformation',25,'add_transformation'),(74,'Can change Transformation',25,'change_transformation'),(75,'Can delete Transformation',25,'delete_transformation'),(76,'Can add Key',26,'add_key'),(77,'Can change Key',26,'change_key'),(78,'Can delete Key',26,'delete_key'),(79,'Can add Lock',27,'add_lock'),(80,'Can change Lock',27,'change_lock'),(81,'Can delete Lock',27,'delete_lock'),(82,'Can add Permission',28,'add_storedpermission'),(83,'Can change Permission',28,'change_storedpermission'),(84,'Can delete Permission',28,'delete_storedpermission'),(85,'Can add Role',29,'add_role'),(86,'Can change Role',29,'change_role'),(87,'Can delete Role',29,'delete_role'),(88,'Can add Cabinet',30,'add_cabinet'),(89,'Can change Cabinet',30,'change_cabinet'),(90,'Can delete Cabinet',30,'delete_cabinet'),(91,'Can add Document cabinet',30,'add_documentcabinet'),(92,'Can change Document cabinet',30,'change_documentcabinet'),(93,'Can delete Document cabinet',30,'delete_documentcabinet'),(94,'Can add New version block',32,'add_newversionblock'),(95,'Can change New version block',32,'change_newversionblock'),(96,'Can delete New version block',32,'delete_newversionblock'),(97,'Can add Document checkout',33,'add_documentcheckout'),(98,'Can change Document checkout',33,'change_documentcheckout'),(99,'Can delete Document checkout',33,'delete_documentcheckout'),(100,'Can add Comment',34,'add_comment'),(101,'Can change Comment',34,'change_comment'),(102,'Can delete Comment',34,'delete_comment'),(103,'Can add Index',35,'add_index'),(104,'Can change Index',35,'change_index'),(105,'Can delete Index',35,'delete_index'),(106,'Can add Index node template',36,'add_indextemplatenode'),(107,'Can change Index node template',36,'change_indextemplatenode'),(108,'Can delete Index node template',36,'delete_indextemplatenode'),(109,'Can add Index instance',35,'add_indexinstance'),(110,'Can change Index instance',35,'change_indexinstance'),(111,'Can delete Index instance',35,'delete_indexinstance'),(112,'Can add Index node instance',37,'add_indexinstancenode'),(113,'Can change Index node instance',37,'change_indexinstancenode'),(114,'Can delete Index node instance',37,'delete_indexinstancenode'),(115,'Can add Document index node instance',37,'add_documentindexinstancenode'),(116,'Can change Document index node instance',37,'change_documentindexinstancenode'),(117,'Can delete Document index node instance',37,'delete_documentindexinstancenode'),(118,'Can add Document page content',40,'add_documentpagecontent'),(119,'Can change Document page content',40,'change_documentpagecontent'),(120,'Can delete Document page content',40,'delete_documentpagecontent'),(121,'Can add Document version parse error',41,'add_documentversionparseerror'),(122,'Can change Document version parse error',41,'change_documentversionparseerror'),(123,'Can delete Document version parse error',41,'delete_documentversionparseerror'),(124,'Can add Document version',42,'add_documentversion'),(125,'Can change Document version',42,'change_documentversion'),(126,'Can delete Document version',42,'delete_documentversion'),(127,'Can add Duplicated document',43,'add_duplicateddocument'),(128,'Can change Duplicated document',43,'change_duplicateddocument'),(129,'Can delete Duplicated document',43,'delete_duplicateddocument'),(130,'Can add Document type',44,'add_documenttype'),(131,'Can change Document type',44,'change_documenttype'),(132,'Can delete Document type',44,'delete_documenttype'),(133,'Can add Document page',45,'add_documentpage'),(134,'Can change Document page',45,'change_documentpage'),(135,'Can delete Document page',45,'delete_documentpage'),(136,'Can add Document page cached image',46,'add_documentpagecachedimage'),(137,'Can change Document page cached image',46,'change_documentpagecachedimage'),(138,'Can delete Document page cached image',46,'delete_documentpagecachedimage'),(139,'Can add Document page',45,'add_documentpageresult'),(140,'Can change Document page',45,'change_documentpageresult'),(141,'Can delete Document page',45,'delete_documentpageresult'),(142,'Can add Recent document',47,'add_recentdocument'),(143,'Can change Recent document',47,'change_recentdocument'),(144,'Can delete Recent document',47,'delete_recentdocument'),(145,'Can add Quick label',48,'add_documenttypefilename'),(146,'Can change Quick label',48,'change_documenttypefilename'),(147,'Can delete Quick label',48,'delete_documenttypefilename'),(148,'Can add Document',49,'add_document'),(149,'Can change Document',49,'change_document'),(150,'Can delete Document',49,'delete_document'),(151,'Can add deleted document',49,'add_deleteddocument'),(152,'Can change deleted document',49,'change_deleteddocument'),(153,'Can delete deleted document',49,'delete_deleteddocument'),(154,'Can add Event type',52,'add_eventtype'),(155,'Can change Event type',52,'change_eventtype'),(156,'Can delete Event type',52,'delete_eventtype'),(157,'Can add Log entry',53,'add_logentry'),(158,'Can change Log entry',53,'change_logentry'),(159,'Can delete Log entry',53,'delete_logentry'),(160,'Can add User mailer log entry',54,'add_usermailerlogentry'),(161,'Can change User mailer log entry',54,'change_usermailerlogentry'),(162,'Can delete User mailer log entry',54,'delete_usermailerlogentry'),(163,'Can add User mailer',55,'add_usermailer'),(164,'Can change User mailer',55,'change_usermailer'),(165,'Can delete User mailer',55,'delete_usermailer'),(166,'Can add Statistics result',56,'add_statisticresult'),(167,'Can change Statistics result',56,'change_statisticresult'),(168,'Can delete Statistics result',56,'delete_statisticresult'),(169,'Can add Document metadata',57,'add_documentmetadata'),(170,'Can change Document metadata',57,'change_documentmetadata'),(171,'Can delete Document metadata',57,'delete_documentmetadata'),(172,'Can add Metadata type',58,'add_metadatatype'),(173,'Can change Metadata type',58,'change_metadatatype'),(174,'Can delete Metadata type',58,'delete_metadatatype'),(175,'Can add Document type metadata type options',59,'add_documenttypemetadatatype'),(176,'Can change Document type metadata type options',59,'change_documenttypemetadatatype'),(177,'Can delete Document type metadata type options',59,'delete_documenttypemetadatatype'),(178,'Can add Message',60,'add_message'),(179,'Can change Message',60,'change_message'),(180,'Can delete Message',60,'delete_message'),(181,'Can add Document version OCR error',61,'add_documentversionocrerror'),(182,'Can change Document version OCR error',61,'change_documentversionocrerror'),(183,'Can delete Document version OCR error',61,'delete_documentversionocrerror'),(184,'Can add Document page OCR content',62,'add_documentpageocrcontent'),(185,'Can change Document page OCR content',62,'change_documentpageocrcontent'),(186,'Can delete Document page OCR content',62,'delete_documentpageocrcontent'),(187,'Can add Document type settings',63,'add_documenttypesettings'),(188,'Can change Document type settings',63,'change_documenttypesettings'),(189,'Can delete Document type settings',63,'delete_documenttypesettings'),(190,'Can add Log entry',64,'add_sourcelog'),(191,'Can change Log entry',64,'change_sourcelog'),(192,'Can delete Log entry',64,'delete_sourcelog'),(193,'Can add Source',65,'add_source'),(194,'Can change Source',65,'change_source'),(195,'Can delete Source',65,'delete_source'),(196,'Can add Interactive source',66,'add_interactivesource'),(197,'Can change Interactive source',66,'change_interactivesource'),(198,'Can delete Interactive source',66,'delete_interactivesource'),(199,'Can add SANE Scanner',67,'add_sanescanner'),(200,'Can change SANE Scanner',67,'change_sanescanner'),(201,'Can delete SANE Scanner',67,'delete_sanescanner'),(202,'Can add Out of process',68,'add_outofprocesssource'),(203,'Can change Out of process',68,'change_outofprocesssource'),(204,'Can delete Out of process',68,'delete_outofprocesssource'),(205,'Can add Interval source',69,'add_intervalbasemodel'),(206,'Can change Interval source',69,'change_intervalbasemodel'),(207,'Can delete Interval source',69,'delete_intervalbasemodel'),(208,'Can add Email source',70,'add_emailbasemodel'),(209,'Can change Email source',70,'change_emailbasemodel'),(210,'Can delete Email source',70,'delete_emailbasemodel'),(211,'Can add Staging folder',71,'add_stagingfoldersource'),(212,'Can change Staging folder',71,'change_stagingfoldersource'),(213,'Can delete Staging folder',71,'delete_stagingfoldersource'),(214,'Can add IMAP email',72,'add_imapemail'),(215,'Can change IMAP email',72,'change_imapemail'),(216,'Can delete IMAP email',72,'delete_imapemail'),(217,'Can add Watch folder',73,'add_watchfoldersource'),(218,'Can change Watch folder',73,'change_watchfoldersource'),(219,'Can delete Watch folder',73,'delete_watchfoldersource'),(220,'Can add POP email',74,'add_pop3email'),(221,'Can change POP email',74,'change_pop3email'),(222,'Can delete POP email',74,'delete_pop3email'),(223,'Can add Web form',75,'add_webformsource'),(224,'Can change Web form',75,'change_webformsource'),(225,'Can delete Web form',75,'delete_webformsource'),(226,'Can add Tag',76,'add_tag'),(227,'Can change Tag',76,'change_tag'),(228,'Can delete Tag',76,'delete_tag'),(229,'Can add Document tag',76,'add_documenttag'),(230,'Can change Document tag',76,'change_documenttag'),(231,'Can delete Document tag',76,'delete_documenttag');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$30000$WrHF2zMnu1CL$TQYcL9A3kfbEX/h+/Hk/hDJiAGNzpOaWHP0EhyE+ydY=',NULL,1,'admin','','','autoadmin@example.com',1,1,'2019-01-22 03:01:55.479051');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authtoken_token`
--

DROP TABLE IF EXISTS `authtoken_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `authtoken_token_user_id_35299eff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authtoken_token`
--

LOCK TABLES `authtoken_token` WRITE;
/*!40000 ALTER TABLE `authtoken_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `authtoken_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autoadmin_autoadminsingleton`
--

DROP TABLE IF EXISTS `autoadmin_autoadminsingleton`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoadmin_autoadminsingleton` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) DEFAULT NULL,
  `password_hash` varchar(128) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `autoadmin_autoadminsingleton_account_id_9082b87a_fk_auth_user_id` (`account_id`),
  CONSTRAINT `autoadmin_autoadminsingleton_account_id_9082b87a_fk_auth_user_id` FOREIGN KEY (`account_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autoadmin_autoadminsingleton`
--

LOCK TABLES `autoadmin_autoadminsingleton` WRITE;
/*!40000 ALTER TABLE `autoadmin_autoadminsingleton` DISABLE KEYS */;
INSERT INTO `autoadmin_autoadminsingleton` VALUES (1,'NGVy9MAbwT','pbkdf2_sha256$30000$WrHF2zMnu1CL$TQYcL9A3kfbEX/h+/Hk/hDJiAGNzpOaWHP0EhyE+ydY=',1);
/*!40000 ALTER TABLE `autoadmin_autoadminsingleton` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cabinets_cabinet`
--

DROP TABLE IF EXISTS `cabinets_cabinet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabinets_cabinet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(128) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cabinets_cabinet_parent_id_441a7709_uniq` (`parent_id`,`label`),
  KEY `cabinets_cabinet_caf7cc51` (`lft`),
  KEY `cabinets_cabinet_3cfbd988` (`rght`),
  KEY `cabinets_cabinet_656442a0` (`tree_id`),
  KEY `cabinets_cabinet_c9e9a848` (`level`),
  KEY `cabinets_cabinet_6be37982` (`parent_id`),
  CONSTRAINT `cabinets_cabinet_parent_id_0939a3c3_fk_cabinets_cabinet_id` FOREIGN KEY (`parent_id`) REFERENCES `cabinets_cabinet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabinets_cabinet`
--

LOCK TABLES `cabinets_cabinet` WRITE;
/*!40000 ALTER TABLE `cabinets_cabinet` DISABLE KEYS */;
/*!40000 ALTER TABLE `cabinets_cabinet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cabinets_cabinet_documents`
--

DROP TABLE IF EXISTS `cabinets_cabinet_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabinets_cabinet_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cabinet_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cabinets_cabinet_documents_cabinet_id_fb3935b0_uniq` (`cabinet_id`,`document_id`),
  KEY `cabinets_cabinet_d_document_id_0f2a0f7a_fk_documents_document_id` (`document_id`),
  CONSTRAINT `cabinets_cabinet_d_document_id_0f2a0f7a_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`),
  CONSTRAINT `cabinets_cabinet_docu_cabinet_id_5f9ca709_fk_cabinets_cabinet_id` FOREIGN KEY (`cabinet_id`) REFERENCES `cabinets_cabinet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabinets_cabinet_documents`
--

LOCK TABLES `cabinets_cabinet_documents` WRITE;
/*!40000 ALTER TABLE `cabinets_cabinet_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `cabinets_cabinet_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_taskmeta`
--

DROP TABLE IF EXISTS `celery_taskmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_taskmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` longtext,
  `date_done` datetime(6) NOT NULL,
  `traceback` longtext,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `celery_taskmeta_662f707d` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_taskmeta`
--

LOCK TABLES `celery_taskmeta` WRITE;
/*!40000 ALTER TABLE `celery_taskmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_taskmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_tasksetmeta`
--

DROP TABLE IF EXISTS `celery_tasksetmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_tasksetmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) NOT NULL,
  `result` longtext NOT NULL,
  `date_done` datetime(6) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskset_id` (`taskset_id`),
  KEY `celery_tasksetmeta_662f707d` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_tasksetmeta`
--

LOCK TABLES `celery_tasksetmeta` WRITE;
/*!40000 ALTER TABLE `celery_tasksetmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_tasksetmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkouts_documentcheckout`
--

DROP TABLE IF EXISTS `checkouts_documentcheckout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkouts_documentcheckout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checkout_datetime` datetime(6) NOT NULL,
  `expiration_datetime` datetime(6) NOT NULL,
  `block_new_version` tinyint(1) NOT NULL,
  `document_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document_id` (`document_id`),
  KEY `checkouts_documentcheckout_user_id_f3ea15e3_fk_auth_user_id` (`user_id`),
  CONSTRAINT `checkouts_document_document_id_edf8044b_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`),
  CONSTRAINT `checkouts_documentcheckout_user_id_f3ea15e3_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkouts_documentcheckout`
--

LOCK TABLES `checkouts_documentcheckout` WRITE;
/*!40000 ALTER TABLE `checkouts_documentcheckout` DISABLE KEYS */;
/*!40000 ALTER TABLE `checkouts_documentcheckout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkouts_newversionblock`
--

DROP TABLE IF EXISTS `checkouts_newversionblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checkouts_newversionblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `checkouts_newversi_document_id_144b2f2d_fk_documents_document_id` (`document_id`),
  CONSTRAINT `checkouts_newversi_document_id_144b2f2d_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkouts_newversionblock`
--

LOCK TABLES `checkouts_newversionblock` WRITE;
/*!40000 ALTER TABLE `checkouts_newversionblock` DISABLE KEYS */;
/*!40000 ALTER TABLE `checkouts_newversionblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_errorlogentry`
--

DROP TABLE IF EXISTS `common_errorlogentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_errorlogentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(128) NOT NULL,
  `object_id` int(10) unsigned DEFAULT NULL,
  `datetime` datetime(6) NOT NULL,
  `result` longtext,
  `content_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `common_errorl_content_type_id_83b13b7c_fk_django_content_type_id` (`content_type_id`),
  KEY `common_errorlogentry_dfeaaeb4` (`datetime`),
  CONSTRAINT `common_errorl_content_type_id_83b13b7c_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_errorlogentry`
--

LOCK TABLES `common_errorlogentry` WRITE;
/*!40000 ALTER TABLE `common_errorlogentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `common_errorlogentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_shareduploadedfile`
--

DROP TABLE IF EXISTS `common_shareduploadedfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_shareduploadedfile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `datetime` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_shareduploadedfile`
--

LOCK TABLES `common_shareduploadedfile` WRITE;
/*!40000 ALTER TABLE `common_shareduploadedfile` DISABLE KEYS */;
/*!40000 ALTER TABLE `common_shareduploadedfile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_userlocaleprofile`
--

DROP TABLE IF EXISTS `common_userlocaleprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_userlocaleprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timezone` varchar(48) NOT NULL,
  `language` varchar(8) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `common_userlocaleprofile_user_id_1984958f_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_userlocaleprofile`
--

LOCK TABLES `common_userlocaleprofile` WRITE;
/*!40000 ALTER TABLE `common_userlocaleprofile` DISABLE KEYS */;
INSERT INTO `common_userlocaleprofile` VALUES (1,'','',1);
/*!40000 ALTER TABLE `common_userlocaleprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `converter_transformation`
--

DROP TABLE IF EXISTS `converter_transformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `converter_transformation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(10) unsigned NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `arguments` longtext NOT NULL,
  `content_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `converter_transformation_content_type_id_0960d9d2_uniq` (`content_type_id`,`object_id`,`order`),
  KEY `converter_transformation_70a17ffa` (`order`),
  CONSTRAINT `converter_tra_content_type_id_28500072_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `converter_transformation`
--

LOCK TABLES `converter_transformation` WRITE;
/*!40000 ALTER TABLE `converter_transformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `converter_transformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (21,'acls','accesscontrollist'),(8,'actstream','action'),(9,'actstream','follow'),(1,'admin','logentry'),(2,'auth','group'),(3,'auth','permission'),(4,'auth','user'),(20,'authtoken','token'),(10,'autoadmin','autoadminsingleton'),(30,'cabinets','cabinet'),(31,'cabinets','documentcabinet'),(33,'checkouts','documentcheckout'),(32,'checkouts','newversionblock'),(24,'common','errorlogentry'),(22,'common','shareduploadedfile'),(23,'common','userlocaleprofile'),(5,'contenttypes','contenttype'),(25,'converter','transformation'),(11,'corsheaders','corsmodel'),(26,'django_gpg','key'),(13,'djcelery','crontabschedule'),(14,'djcelery','intervalschedule'),(12,'djcelery','periodictask'),(15,'djcelery','periodictasks'),(16,'djcelery','taskmeta'),(17,'djcelery','tasksetmeta'),(19,'djcelery','taskstate'),(18,'djcelery','workerstate'),(50,'documents','deleteddocument'),(49,'documents','document'),(45,'documents','documentpage'),(46,'documents','documentpagecachedimage'),(51,'documents','documentpageresult'),(44,'documents','documenttype'),(48,'documents','documenttypefilename'),(42,'documents','documentversion'),(43,'documents','duplicateddocument'),(47,'documents','recentdocument'),(34,'document_comments','comment'),(39,'document_indexing','documentindexinstancenode'),(35,'document_indexing','index'),(38,'document_indexing','indexinstance'),(37,'document_indexing','indexinstancenode'),(36,'document_indexing','indextemplatenode'),(40,'document_parsing','documentpagecontent'),(41,'document_parsing','documentversionparseerror'),(52,'events','eventtype'),(27,'lock_manager','lock'),(53,'mailer','logentry'),(55,'mailer','usermailer'),(54,'mailer','usermailerlogentry'),(56,'mayan_statistics','statisticresult'),(57,'metadata','documentmetadata'),(59,'metadata','documenttypemetadatatype'),(58,'metadata','metadatatype'),(60,'motd','message'),(62,'ocr','documentpageocrcontent'),(63,'ocr','documenttypesettings'),(61,'ocr','documentversionocrerror'),(29,'permissions','role'),(28,'permissions','storedpermission'),(6,'sessions','session'),(7,'sites','site'),(70,'sources','emailbasemodel'),(72,'sources','imapemail'),(66,'sources','interactivesource'),(69,'sources','intervalbasemodel'),(68,'sources','outofprocesssource'),(74,'sources','pop3email'),(67,'sources','sanescanner'),(65,'sources','source'),(64,'sources','sourcelog'),(71,'sources','stagingfoldersource'),(73,'sources','watchfoldersource'),(75,'sources','webformsource'),(77,'tags','documenttag'),(76,'tags','tag');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_gpg_key`
--

DROP TABLE IF EXISTS `django_gpg_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_gpg_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` date NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `fingerprint` varchar(40) NOT NULL,
  `length` int(10) unsigned NOT NULL,
  `algorithm` int(10) unsigned NOT NULL,
  `user_id` longtext NOT NULL,
  `key_type` varchar(3) NOT NULL,
  `key_data` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fingerprint` (`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_gpg_key`
--

LOCK TABLES `django_gpg_key` WRITE;
/*!40000 ALTER TABLE `django_gpg_key` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_gpg_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-01-22 03:01:13.557640'),(2,'permissions','0001_initial','2019-01-22 03:01:14.375429'),(3,'auth','0001_initial','2019-01-22 03:01:16.029641'),(4,'permissions','0002_auto_20150628_0533','2019-01-22 03:01:17.377389'),(5,'acls','0001_initial','2019-01-22 03:01:18.012073'),(6,'acls','0002_auto_20150703_0513','2019-01-22 03:01:19.524050'),(7,'actstream','0001_initial','2019-01-22 03:01:20.609263'),(8,'actstream','0002_remove_action_data','2019-01-22 03:01:20.826261'),(9,'admin','0001_initial','2019-01-22 03:01:21.111730'),(10,'admin','0002_logentry_remove_auto_add','2019-01-22 03:01:21.156801'),(11,'contenttypes','0002_remove_content_type_name','2019-01-22 03:01:21.462248'),(12,'auth','0002_alter_permission_name_max_length','2019-01-22 03:01:21.571137'),(13,'auth','0003_alter_user_email_max_length','2019-01-22 03:01:21.704698'),(14,'auth','0004_alter_user_username_opts','2019-01-22 03:01:21.734139'),(15,'auth','0005_alter_user_last_login_null','2019-01-22 03:01:21.824811'),(16,'auth','0006_require_contenttypes_0002','2019-01-22 03:01:21.832680'),(17,'auth','0007_alter_validators_add_error_messages','2019-01-22 03:01:21.856145'),(18,'auth','0008_alter_user_username_max_length','2019-01-22 03:01:21.994286'),(19,'authtoken','0001_initial','2019-01-22 03:01:22.169551'),(20,'autoadmin','0001_initial','2019-01-22 03:01:22.327132'),(21,'documents','0001_initial','2019-01-22 03:01:23.810947'),(22,'ocr','0001_initial','2019-01-22 03:01:24.033156'),(23,'documents','0002_auto_20150608_1902','2019-01-22 03:01:24.170271'),(24,'documents','0003_auto_20150608_1915','2019-01-22 03:01:24.270615'),(25,'documents','0004_auto_20150616_1930','2019-01-22 03:01:24.396451'),(26,'documents','0005_auto_20150617_0358','2019-01-22 03:01:24.459651'),(27,'ocr','0002_documentpagecontent','2019-01-22 03:01:24.648709'),(28,'ocr','0003_auto_20150617_0401','2019-01-22 03:01:24.667562'),(29,'documents','0006_remove_documentpage_content_old','2019-01-22 03:01:24.770895'),(30,'documents','0007_remove_documentpage_page_label','2019-01-22 03:01:24.952953'),(31,'documents','0008_auto_20150624_0520','2019-01-22 03:01:25.092402'),(32,'documents','0009_document_in_trash','2019-01-22 03:01:25.216461'),(33,'documents','0010_auto_20150704_0054','2019-01-22 03:01:25.362808'),(34,'documents','0011_auto_20150704_0508','2019-01-22 03:01:25.915343'),(35,'documents','0012_auto_20150705_0347','2019-01-22 03:01:26.159907'),(36,'documents','0013_document_is_stub','2019-01-22 03:01:26.305589'),(37,'documents','0014_auto_20150708_0107','2019-01-22 03:01:26.391247'),(38,'documents','0015_auto_20150708_0113','2019-01-22 03:01:26.431455'),(39,'documents','0016_auto_20150708_0325','2019-01-22 03:01:26.557075'),(40,'documents','0017_auto_20150714_0056','2019-01-22 03:01:26.734984'),(41,'documents','0018_auto_20150714_2227','2019-01-22 03:01:26.953842'),(42,'documents','0019_auto_20150714_2232','2019-01-22 03:01:26.988613'),(43,'documents','0020_auto_20150714_2233','2019-01-22 03:01:27.376233'),(44,'documents','0021_auto_20150714_2238','2019-01-22 03:01:27.488924'),(45,'documents','0022_auto_20150715_0258','2019-01-22 03:01:27.630837'),(46,'documents','0023_auto_20150715_0259','2019-01-22 03:01:27.937121'),(47,'documents','0024_auto_20150715_0714','2019-01-22 03:01:28.056831'),(48,'documents','0025_auto_20150718_0742','2019-01-22 03:01:28.069604'),(49,'documents','0026_auto_20150729_2140','2019-01-22 03:01:28.106009'),(50,'documents','0027_auto_20150824_0702','2019-01-22 03:01:28.204912'),(51,'documents','0028_newversionblock','2019-01-22 03:01:28.377306'),(52,'documents','0029_auto_20160122_0755','2019-01-22 03:01:28.413463'),(53,'documents','0030_auto_20160309_1837','2019-01-22 03:01:28.604902'),(54,'documents','0031_convert_uuid','2019-01-22 03:01:28.617556'),(55,'documents','0032_auto_20160315_0537','2019-01-22 03:01:28.624730'),(56,'documents','0033_auto_20160325_0052','2019-01-22 03:01:28.660258'),(57,'documents','0034_auto_20160509_2321','2019-01-22 03:01:28.851895'),(58,'cabinets','0001_initial','2019-01-22 03:01:29.628511'),(59,'documents','0035_auto_20161102_0633','2019-01-22 03:01:29.986420'),(60,'documents','0036_auto_20161222_0534','2019-01-22 03:01:30.143656'),(61,'checkouts','0001_initial','2019-01-22 03:01:30.424556'),(62,'checkouts','0002_documentcheckout_user','2019-01-22 03:01:30.710700'),(63,'checkouts','0003_auto_20150617_0325','2019-01-22 03:01:30.723621'),(64,'checkouts','0004_auto_20150617_0330','2019-01-22 03:01:31.232821'),(65,'checkouts','0005_auto_20160122_0756','2019-01-22 03:01:31.467545'),(66,'checkouts','0006_newversionblock','2019-01-22 03:01:31.674956'),(67,'common','0001_initial','2019-01-22 03:01:32.244952'),(68,'common','0002_auto_20150608_1902','2019-01-22 03:01:32.289929'),(69,'common','0003_auto_20150614_0723','2019-01-22 03:01:32.424144'),(70,'common','0004_delete_anonymoususersingleton','2019-01-22 03:01:32.441867'),(71,'common','0005_auto_20150706_1832','2019-01-22 03:01:32.503167'),(72,'common','0006_auto_20160313_0313','2019-01-22 03:01:32.544970'),(73,'common','0007_auto_20170118_1758','2019-01-22 03:01:32.663390'),(74,'common','0008_errorlogentry','2019-01-22 03:01:32.862634'),(75,'converter','0001_initial','2019-01-22 03:01:33.072851'),(76,'converter','0002_auto_20150608_1943','2019-01-22 03:01:33.146528'),(77,'converter','0003_auto_20150704_0731','2019-01-22 03:01:33.360124'),(78,'converter','0004_auto_20150704_0753','2019-01-22 03:01:33.404125'),(79,'converter','0005_auto_20150708_0118','2019-01-22 03:01:33.492965'),(80,'converter','0006_auto_20150708_0120','2019-01-22 03:01:33.536978'),(81,'converter','0007_auto_20150711_0656','2019-01-22 03:01:33.583495'),(82,'converter','0008_auto_20150711_0723','2019-01-22 03:01:33.794147'),(83,'converter','0009_auto_20150714_2228','2019-01-22 03:01:33.840203'),(84,'converter','0010_auto_20150815_0351','2019-01-22 03:01:33.885991'),(85,'converter','0011_auto_20170118_1758','2019-01-22 03:01:33.930237'),(86,'converter','0012_auto_20170714_2133','2019-01-22 03:01:34.062912'),(87,'django_gpg','0001_initial','2019-01-22 03:01:34.145153'),(88,'django_gpg','0002_auto_20160322_1756','2019-01-22 03:01:34.375179'),(89,'django_gpg','0003_auto_20160322_1810','2019-01-22 03:01:34.438711'),(90,'django_gpg','0004_auto_20160322_2202','2019-01-22 03:01:34.455315'),(91,'django_gpg','0005_remove_key_key_id','2019-01-22 03:01:34.531109'),(92,'django_gpg','0006_auto_20160510_0025','2019-01-22 03:01:34.540741'),(93,'djcelery','0001_initial','2019-01-22 03:01:36.039960'),(94,'sites','0001_initial','2019-01-22 03:01:36.128429'),(95,'document_comments','0001_initial','2019-01-22 03:01:36.492454'),(96,'document_comments','0002_auto_20150729_2144','2019-01-22 03:01:36.609436'),(97,'document_comments','0003_auto_20150729_2144','2019-01-22 03:01:36.618414'),(98,'document_comments','0004_auto_20150920_0202','2019-01-22 03:01:36.668253'),(99,'document_indexing','0001_initial','2019-01-22 03:01:38.906169'),(100,'document_indexing','0002_remove_index_name','2019-01-22 03:01:39.017599'),(101,'document_indexing','0003_auto_20150708_0101','2019-01-22 03:01:39.091100'),(102,'document_indexing','0004_auto_20150708_0113','2019-01-22 03:01:39.143748'),(103,'document_indexing','0005_index_slug','2019-01-22 03:01:39.346320'),(104,'document_indexing','0006_auto_20150729_0144','2019-01-22 03:01:39.355889'),(105,'document_indexing','0007_auto_20150729_0152','2019-01-22 03:01:39.527723'),(106,'document_indexing','0008_auto_20150729_1515','2019-01-22 03:01:39.603479'),(107,'document_indexing','0009_auto_20150815_0351','2019-01-22 03:01:39.666284'),(108,'document_indexing','0010_documentindexinstancenode_indexinstance','2019-01-22 03:01:39.684628'),(109,'document_indexing','0011_auto_20170524_0456','2019-01-22 03:01:39.887765'),(110,'document_indexing','0012_auto_20170530_0728','2019-01-22 03:01:40.653294'),(111,'document_indexing','0013_auto_20170714_2133','2019-01-22 03:01:40.714891'),(112,'documents','0037_auto_20161231_0617','2019-01-22 03:01:40.770691'),(113,'documents','0038_auto_20170705_2008','2019-01-22 03:01:40.946761'),(114,'documents','0039_duplicateddocument','2019-01-22 03:01:41.493395'),(115,'documents','0040_auto_20170725_1111','2019-01-22 03:01:41.556980'),(116,'documents','0041_auto_20170823_1855','2019-01-22 03:01:41.868927'),(117,'document_parsing','0001_initial','2019-01-22 03:01:42.329351'),(118,'document_parsing','0002_auto_20170827_1617','2019-01-22 03:01:42.496198'),(119,'dynamic_search','0001_initial','2019-01-22 03:01:42.719778'),(120,'dynamic_search','0002_auto_20150920_0202','2019-01-22 03:01:42.888463'),(121,'dynamic_search','0003_auto_20161028_0707','2019-01-22 03:01:43.055036'),(122,'events','0001_initial','2019-01-22 03:01:43.134680'),(123,'lock_manager','0001_initial','2019-01-22 03:01:43.209443'),(124,'lock_manager','0002_auto_20150604_2219','2019-01-22 03:01:43.247162'),(125,'mailer','0001_initial','2019-01-22 03:01:43.325472'),(126,'mailer','0002_usermailer_usermailerlogentry','2019-01-22 03:01:43.618947'),(127,'mailer','0003_auto_20170703_1535','2019-01-22 03:01:43.907293'),(128,'mailer','0004_auto_20170714_2133','2019-01-22 03:01:43.925455'),(129,'mailer','0005_auto_20170718_0123','2019-01-22 03:01:44.013248'),(130,'mayan_statistics','0001_initial','2019-01-22 03:01:44.099374'),(131,'metadata','0001_initial','2019-01-22 03:01:45.519309'),(132,'metadata','0002_auto_20150708_0118','2019-01-22 03:01:45.612813'),(133,'metadata','0003_auto_20150708_0323','2019-01-22 03:01:45.763157'),(134,'metadata','0004_auto_20150708_0324','2019-01-22 03:01:45.825659'),(135,'metadata','0005_auto_20150729_2344','2019-01-22 03:01:46.011263'),(136,'metadata','0006_auto_20150820_0616','2019-01-22 03:01:46.218639'),(137,'metadata','0007_auto_20150918_0800','2019-01-22 03:01:46.355849'),(138,'motd','0001_initial','2019-01-22 03:01:46.448640'),(139,'motd','0002_auto_20160313_0340','2019-01-22 03:01:46.580745'),(140,'motd','0003_auto_20160313_0349','2019-01-22 03:01:46.593191'),(141,'motd','0004_auto_20160314_0040','2019-01-22 03:01:46.633568'),(142,'motd','0005_auto_20160510_0025','2019-01-22 03:01:46.672609'),(143,'ocr','0004_documenttypesettings','2019-01-22 03:01:46.906701'),(144,'ocr','0005_auto_20170630_1846','2019-01-22 03:01:47.110648'),(145,'ocr','0006_auto_20170823_0553','2019-01-22 03:01:47.637271'),(146,'ocr','0007_auto_20170827_1617','2019-01-22 03:01:47.781896'),(147,'permissions','0003_remove_role_name','2019-01-22 03:01:47.916403'),(148,'sessions','0001_initial','2019-01-22 03:01:48.082752'),(149,'sites','0002_alter_domain_unique','2019-01-22 03:01:48.120527'),(150,'sources','0001_initial','2019-01-22 03:01:50.020305'),(151,'sources','0002_auto_20150608_1902','2019-01-22 03:01:50.184102'),(152,'sources','0003_sourcelog','2019-01-22 03:01:50.444006'),(153,'sources','0004_auto_20150616_1931','2019-01-22 03:01:50.549317'),(154,'sources','0005_auto_20150708_0327','2019-01-22 03:01:50.718761'),(155,'sources','0006_auto_20150708_0330','2019-01-22 03:01:50.883642'),(156,'sources','0007_emailbasemodel_metadata_attachment_name','2019-01-22 03:01:51.063896'),(157,'sources','0008_auto_20150815_0351','2019-01-22 03:01:51.142279'),(158,'sources','0009_auto_20150930_2341','2019-01-22 03:01:51.676658'),(159,'sources','0010_auto_20151001_0055','2019-01-22 03:01:51.972731'),(160,'sources','0011_sanescanner','2019-01-22 03:01:52.175133'),(161,'sources','0012_auto_20170205_0743','2019-01-22 03:01:52.495849'),(162,'sources','0013_auto_20170206_0710','2019-01-22 03:01:53.037864'),(163,'sources','0014_auto_20170206_0722','2019-01-22 03:01:53.206053'),(164,'sources','0015_auto_20170206_0835','2019-01-22 03:01:53.294467'),(165,'sources','0016_auto_20170630_2040','2019-01-22 03:01:53.434107'),(166,'tags','0001_initial','2019-01-22 03:01:53.925516'),(167,'tags','0002_tag_selection','2019-01-22 03:01:54.199765'),(168,'tags','0003_remove_tag_color','2019-01-22 03:01:54.344287'),(169,'tags','0004_auto_20150717_2336','2019-01-22 03:01:54.443443'),(170,'tags','0005_auto_20150718_0616','2019-01-22 03:01:54.532226'),(171,'tags','0006_documenttag','2019-01-22 03:01:54.544199'),(172,'tags','0007_auto_20170118_1758','2019-01-22 03:01:54.626065');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_crontabschedule`
--

DROP TABLE IF EXISTS `djcelery_crontabschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_crontabschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) NOT NULL,
  `hour` varchar(64) NOT NULL,
  `day_of_week` varchar(64) NOT NULL,
  `day_of_month` varchar(64) NOT NULL,
  `month_of_year` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_crontabschedule`
--

LOCK TABLES `djcelery_crontabschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_crontabschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_crontabschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_intervalschedule`
--

DROP TABLE IF EXISTS `djcelery_intervalschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_intervalschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_intervalschedule`
--

LOCK TABLES `djcelery_intervalschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_intervalschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_intervalschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictask`
--

DROP TABLE IF EXISTS `djcelery_periodictask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `task` varchar(200) NOT NULL,
  `args` longtext NOT NULL,
  `kwargs` longtext NOT NULL,
  `queue` varchar(200) DEFAULT NULL,
  `exchange` varchar(200) DEFAULT NULL,
  `routing_key` varchar(200) DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime(6) DEFAULT NULL,
  `total_run_count` int(10) unsigned NOT NULL,
  `date_changed` datetime(6) NOT NULL,
  `description` longtext NOT NULL,
  `crontab_id` int(11) DEFAULT NULL,
  `interval_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `djcelery_peri_crontab_id_75609bab_fk_djcelery_crontabschedule_id` (`crontab_id`),
  KEY `djcelery_pe_interval_id_b426ab02_fk_djcelery_intervalschedule_id` (`interval_id`),
  CONSTRAINT `djcelery_pe_interval_id_b426ab02_fk_djcelery_intervalschedule_id` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`),
  CONSTRAINT `djcelery_peri_crontab_id_75609bab_fk_djcelery_crontabschedule_id` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictask`
--

LOCK TABLES `djcelery_periodictask` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictask` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictasks`
--

DROP TABLE IF EXISTS `djcelery_periodictasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictasks` (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime(6) NOT NULL,
  PRIMARY KEY (`ident`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictasks`
--

LOCK TABLES `djcelery_periodictasks` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_taskstate`
--

DROP TABLE IF EXISTS `djcelery_taskstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_taskstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) NOT NULL,
  `task_id` varchar(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tstamp` datetime(6) NOT NULL,
  `args` longtext,
  `kwargs` longtext,
  `eta` datetime(6) DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `result` longtext,
  `traceback` longtext,
  `runtime` double DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `worker_id` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `djcelery_taskstate_9ed39e2e` (`state`),
  KEY `djcelery_taskstate_b068931c` (`name`),
  KEY `djcelery_taskstate_863bb2ee` (`tstamp`),
  KEY `djcelery_taskstate_662f707d` (`hidden`),
  KEY `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` (`worker_id`),
  CONSTRAINT `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_taskstate`
--

LOCK TABLES `djcelery_taskstate` WRITE;
/*!40000 ALTER TABLE `djcelery_taskstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_taskstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_workerstate`
--

DROP TABLE IF EXISTS `djcelery_workerstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_workerstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `last_heartbeat` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `djcelery_workerstate_f129901a` (`last_heartbeat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_workerstate`
--

LOCK TABLES `djcelery_workerstate` WRITE;
/*!40000 ALTER TABLE `djcelery_workerstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_workerstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_comments_comment`
--

DROP TABLE IF EXISTS `document_comments_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_comments_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` longtext NOT NULL,
  `submit_date` datetime(6) NOT NULL,
  `document_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `document_comments__document_id_2435cfe5_fk_documents_document_id` (`document_id`),
  KEY `document_comments_comment_user_id_cfa5a004_fk_auth_user_id` (`user_id`),
  KEY `document_comments_comment_d21e9f6c` (`submit_date`),
  CONSTRAINT `document_comments__document_id_2435cfe5_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`),
  CONSTRAINT `document_comments_comment_user_id_cfa5a004_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_comments_comment`
--

LOCK TABLES `document_comments_comment` WRITE;
/*!40000 ALTER TABLE `document_comments_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_comments_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_indexing_index`
--

DROP TABLE IF EXISTS `document_indexing_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indexing_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `slug` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`label`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_indexing_index`
--

LOCK TABLES `document_indexing_index` WRITE;
/*!40000 ALTER TABLE `document_indexing_index` DISABLE KEYS */;
INSERT INTO `document_indexing_index` VALUES (1,'Creation date',1,'creation_date');
/*!40000 ALTER TABLE `document_indexing_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_indexing_index_document_types`
--

DROP TABLE IF EXISTS `document_indexing_index_document_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indexing_index_document_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `index_id` int(11) NOT NULL,
  `documenttype_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document_indexing_index_document_types_index_id_191c4d4b_uniq` (`index_id`,`documenttype_id`),
  KEY `document_i_documenttype_id_e787546a_fk_documents_documenttype_id` (`documenttype_id`),
  CONSTRAINT `document_i_documenttype_id_e787546a_fk_documents_documenttype_id` FOREIGN KEY (`documenttype_id`) REFERENCES `documents_documenttype` (`id`),
  CONSTRAINT `document_indexin_index_id_bb64891c_fk_document_indexing_index_id` FOREIGN KEY (`index_id`) REFERENCES `document_indexing_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_indexing_index_document_types`
--

LOCK TABLES `document_indexing_index_document_types` WRITE;
/*!40000 ALTER TABLE `document_indexing_index_document_types` DISABLE KEYS */;
INSERT INTO `document_indexing_index_document_types` VALUES (1,1,1);
/*!40000 ALTER TABLE `document_indexing_index_document_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_indexing_indexinstancenode`
--

DROP TABLE IF EXISTS `document_indexing_indexinstancenode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indexing_indexinstancenode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(128) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `index_template_node_id` int(11) NOT NULL,
  `parent_id` int(11),
  PRIMARY KEY (`id`),
  KEY `document_indexing_indexinstancenode_caf7cc51` (`lft`),
  KEY `document_indexing_indexinstancenode_3cfbd988` (`rght`),
  KEY `document_indexing_indexinstancenode_656442a0` (`tree_id`),
  KEY `document_indexing_indexinstancenode_c9e9a848` (`level`),
  KEY `document_indexing_indexinstancenode_6be37982` (`parent_id`),
  KEY `document_indexing_indexinstancenode_value_cf7fc740_uniq` (`value`),
  KEY `a24cf439f8f974cb8c403b9dff329230` (`index_template_node_id`),
  CONSTRAINT `a24cf439f8f974cb8c403b9dff329230` FOREIGN KEY (`index_template_node_id`) REFERENCES `document_indexing_indextemplatenode` (`id`),
  CONSTRAINT `doc_parent_id_3db469f0_fk_document_indexing_indexinstancenode_id` FOREIGN KEY (`parent_id`) REFERENCES `document_indexing_indexinstancenode` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_indexing_indexinstancenode`
--

LOCK TABLES `document_indexing_indexinstancenode` WRITE;
/*!40000 ALTER TABLE `document_indexing_indexinstancenode` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_indexing_indexinstancenode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_indexing_indexinstancenode_documents`
--

DROP TABLE IF EXISTS `document_indexing_indexinstancenode_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indexing_indexinstancenode_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indexinstancenode_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document_indexing_indexinstan_indexinstancenode_id_6bb4c624_uniq` (`indexinstancenode_id`,`document_id`),
  KEY `document_indexing__document_id_01ea67cd_fk_documents_document_id` (`document_id`),
  CONSTRAINT `D369df7704539dd3c1dd2cae8b43a81b` FOREIGN KEY (`indexinstancenode_id`) REFERENCES `document_indexing_indexinstancenode` (`id`),
  CONSTRAINT `document_indexing__document_id_01ea67cd_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_indexing_indexinstancenode_documents`
--

LOCK TABLES `document_indexing_indexinstancenode_documents` WRITE;
/*!40000 ALTER TABLE `document_indexing_indexinstancenode_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_indexing_indexinstancenode_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_indexing_indextemplatenode`
--

DROP TABLE IF EXISTS `document_indexing_indextemplatenode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_indexing_indextemplatenode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expression` longtext NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `link_documents` tinyint(1) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `index_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `document_indexin_index_id_1a5de5b2_fk_document_indexing_index_id` (`index_id`),
  KEY `document_indexing_indextemplatenode_caf7cc51` (`lft`),
  KEY `document_indexing_indextemplatenode_3cfbd988` (`rght`),
  KEY `document_indexing_indextemplatenode_656442a0` (`tree_id`),
  KEY `document_indexing_indextemplatenode_c9e9a848` (`level`),
  KEY `document_indexing_indextemplatenode_6be37982` (`parent_id`),
  CONSTRAINT `doc_parent_id_27089927_fk_document_indexing_indextemplatenode_id` FOREIGN KEY (`parent_id`) REFERENCES `document_indexing_indextemplatenode` (`id`),
  CONSTRAINT `document_indexin_index_id_1a5de5b2_fk_document_indexing_index_id` FOREIGN KEY (`index_id`) REFERENCES `document_indexing_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_indexing_indextemplatenode`
--

LOCK TABLES `document_indexing_indextemplatenode` WRITE;
/*!40000 ALTER TABLE `document_indexing_indextemplatenode` DISABLE KEYS */;
INSERT INTO `document_indexing_indextemplatenode` VALUES (1,'',1,0,1,6,1,0,1,NULL),(2,'{{ document.date_added|date:\"Y\" }}',1,0,2,5,1,1,1,1),(3,'{{ document.date_added|date:\"m\" }}',1,1,3,4,1,2,1,2);
/*!40000 ALTER TABLE `document_indexing_indextemplatenode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_parsing_documentpagecontent`
--

DROP TABLE IF EXISTS `document_parsing_documentpagecontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_parsing_documentpagecontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `document_page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document_page_id` (`document_page_id`),
  CONSTRAINT `document__document_page_id_ad640195_fk_documents_documentpage_id` FOREIGN KEY (`document_page_id`) REFERENCES `documents_documentpage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_parsing_documentpagecontent`
--

LOCK TABLES `document_parsing_documentpagecontent` WRITE;
/*!40000 ALTER TABLE `document_parsing_documentpagecontent` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_parsing_documentpagecontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_parsing_documentversionparseerror`
--

DROP TABLE IF EXISTS `document_parsing_documentversionparseerror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_parsing_documentversionparseerror` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime_submitted` datetime(6) NOT NULL,
  `result` longtext,
  `document_version_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `document_parsing_documentversionparseerror_98960ed2` (`datetime_submitted`),
  KEY `doc_document_version_id_841beea8_fk_documents_documentversion_id` (`document_version_id`),
  CONSTRAINT `doc_document_version_id_841beea8_fk_documents_documentversion_id` FOREIGN KEY (`document_version_id`) REFERENCES `documents_documentversion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_parsing_documentversionparseerror`
--

LOCK TABLES `document_parsing_documentversionparseerror` WRITE;
/*!40000 ALTER TABLE `document_parsing_documentversionparseerror` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_parsing_documentversionparseerror` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_document`
--

DROP TABLE IF EXISTS `documents_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` char(32) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `date_added` datetime(6) NOT NULL,
  `language` varchar(8) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `in_trash` tinyint(1) NOT NULL,
  `deleted_date_time` datetime(6) DEFAULT NULL,
  `is_stub` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_document_d304ba20` (`label`),
  KEY `documents_document_type_id_1f88b50c_fk_documents_documenttype_id` (`document_type_id`),
  KEY `documents_document_date_added_873349cc_uniq` (`date_added`),
  KEY `documents_document_in_trash_99a07799_uniq` (`in_trash`),
  KEY `documents_document_is_stub_7a1cdd11_uniq` (`is_stub`),
  CONSTRAINT `documents_document_type_id_1f88b50c_fk_documents_documenttype_id` FOREIGN KEY (`document_type_id`) REFERENCES `documents_documenttype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_document`
--

LOCK TABLES `documents_document` WRITE;
/*!40000 ALTER TABLE `documents_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_documentpage`
--

DROP TABLE IF EXISTS `documents_documentpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_documentpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_number` int(10) unsigned NOT NULL,
  `document_version_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_documentpage_90cb6211` (`page_number`),
  KEY `doc_document_version_id_5d1c2629_fk_documents_documentversion_id` (`document_version_id`),
  CONSTRAINT `doc_document_version_id_5d1c2629_fk_documents_documentversion_id` FOREIGN KEY (`document_version_id`) REFERENCES `documents_documentversion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_documentpage`
--

LOCK TABLES `documents_documentpage` WRITE;
/*!40000 ALTER TABLE `documents_documentpage` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_documentpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_documentpagecachedimage`
--

DROP TABLE IF EXISTS `documents_documentpagecachedimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_documentpagecachedimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(128) NOT NULL,
  `document_page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_document_page_id_74c2cc5e_fk_documents_documentpage_id` (`document_page_id`),
  CONSTRAINT `documents_document_page_id_74c2cc5e_fk_documents_documentpage_id` FOREIGN KEY (`document_page_id`) REFERENCES `documents_documentpage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_documentpagecachedimage`
--

LOCK TABLES `documents_documentpagecachedimage` WRITE;
/*!40000 ALTER TABLE `documents_documentpagecachedimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_documentpagecachedimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_documenttype`
--

DROP TABLE IF EXISTS `documents_documenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_documenttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(32) NOT NULL,
  `delete_time_period` int(10) unsigned DEFAULT NULL,
  `delete_time_unit` varchar(8) DEFAULT NULL,
  `trash_time_period` int(10) unsigned,
  `trash_time_unit` varchar(8),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_documenttype`
--

LOCK TABLES `documents_documenttype` WRITE;
/*!40000 ALTER TABLE `documents_documenttype` DISABLE KEYS */;
INSERT INTO `documents_documenttype` VALUES (1,'Default',30,'days',NULL,NULL);
/*!40000 ALTER TABLE `documents_documenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_documenttypefilename`
--

DROP TABLE IF EXISTS `documents_documenttypefilename`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_documenttypefilename` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(128) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `documents_documenttypefilename_document_type_id_cffa5dbb_uniq` (`document_type_id`,`filename`),
  KEY `documents_documenttypefilename_435ed7e9` (`filename`),
  CONSTRAINT `documents_document_type_id_56f21f7f_fk_documents_documenttype_id` FOREIGN KEY (`document_type_id`) REFERENCES `documents_documenttype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_documenttypefilename`
--

LOCK TABLES `documents_documenttypefilename` WRITE;
/*!40000 ALTER TABLE `documents_documenttypefilename` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_documenttypefilename` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_documentversion`
--

DROP TABLE IF EXISTS `documents_documentversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_documentversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime(6) NOT NULL,
  `comment` longtext NOT NULL,
  `file` varchar(100) NOT NULL,
  `mimetype` varchar(255) DEFAULT NULL,
  `encoding` varchar(64) DEFAULT NULL,
  `checksum` varchar(64) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_document_document_id_42757b7a_fk_documents_document_id` (`document_id`),
  KEY `documents_documentversion_timestamp_30bada95_uniq` (`timestamp`),
  KEY `documents_documentversion_checksum_474801cd_uniq` (`checksum`),
  CONSTRAINT `documents_document_document_id_42757b7a_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_documentversion`
--

LOCK TABLES `documents_documentversion` WRITE;
/*!40000 ALTER TABLE `documents_documentversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_documentversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_duplicateddocument`
--

DROP TABLE IF EXISTS `documents_duplicateddocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_duplicateddocument` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime_added` datetime(6) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_duplicat_document_id_67eaf8d5_fk_documents_document_id` (`document_id`),
  KEY `documents_duplicateddocument_86cc3f16` (`datetime_added`),
  CONSTRAINT `documents_duplicat_document_id_67eaf8d5_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_duplicateddocument`
--

LOCK TABLES `documents_duplicateddocument` WRITE;
/*!40000 ALTER TABLE `documents_duplicateddocument` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_duplicateddocument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_duplicateddocument_documents`
--

DROP TABLE IF EXISTS `documents_duplicateddocument_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_duplicateddocument_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `duplicateddocument_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `documents_duplicateddocument_duplicateddocument_id_57806cab_uniq` (`duplicateddocument_id`,`document_id`),
  KEY `documents_duplicat_document_id_74a2ee3c_fk_documents_document_id` (`document_id`),
  CONSTRAINT `dd85df46c775d0835a94bcc9ae262578` FOREIGN KEY (`duplicateddocument_id`) REFERENCES `documents_duplicateddocument` (`id`),
  CONSTRAINT `documents_duplicat_document_id_74a2ee3c_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_duplicateddocument_documents`
--

LOCK TABLES `documents_duplicateddocument_documents` WRITE;
/*!40000 ALTER TABLE `documents_duplicateddocument_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_duplicateddocument_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_recentdocument`
--

DROP TABLE IF EXISTS `documents_recentdocument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_recentdocument` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime_accessed` datetime(6) NOT NULL,
  `document_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_recentdo_document_id_cbe05f47_fk_documents_document_id` (`document_id`),
  KEY `documents_recentdocument_user_id_705045b9_fk_auth_user_id` (`user_id`),
  KEY `documents_recentdocument_510e3ecd` (`datetime_accessed`),
  CONSTRAINT `documents_recentdo_document_id_cbe05f47_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`),
  CONSTRAINT `documents_recentdocument_user_id_705045b9_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_recentdocument`
--

LOCK TABLES `documents_recentdocument` WRITE;
/*!40000 ALTER TABLE `documents_recentdocument` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents_recentdocument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_eventtype`
--

DROP TABLE IF EXISTS `events_eventtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_eventtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_eventtype`
--

LOCK TABLES `events_eventtype` WRITE;
/*!40000 ALTER TABLE `events_eventtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_eventtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lock_manager_lock`
--

DROP TABLE IF EXISTS `lock_manager_lock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lock_manager_lock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_datetime` datetime(6) NOT NULL,
  `timeout` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lock_manager_lock`
--

LOCK TABLES `lock_manager_lock` WRITE;
/*!40000 ALTER TABLE `lock_manager_lock` DISABLE KEYS */;
/*!40000 ALTER TABLE `lock_manager_lock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailer_logentry`
--

DROP TABLE IF EXISTS `mailer_logentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailer_logentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime(6) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailer_logentry`
--

LOCK TABLES `mailer_logentry` WRITE;
/*!40000 ALTER TABLE `mailer_logentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_logentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailer_usermailer`
--

DROP TABLE IF EXISTS `mailer_usermailer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailer_usermailer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(128) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `backend_path` varchar(128) NOT NULL,
  `backend_data` longtext NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailer_usermailer`
--

LOCK TABLES `mailer_usermailer` WRITE;
/*!40000 ALTER TABLE `mailer_usermailer` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_usermailer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mailer_usermailerlogentry`
--

DROP TABLE IF EXISTS `mailer_usermailerlogentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mailer_usermailerlogentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime(6) NOT NULL,
  `message` longtext NOT NULL,
  `user_mailer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mailer_usermaile_user_mailer_id_ad4f227e_fk_mailer_usermailer_id` (`user_mailer_id`),
  CONSTRAINT `mailer_usermaile_user_mailer_id_ad4f227e_fk_mailer_usermailer_id` FOREIGN KEY (`user_mailer_id`) REFERENCES `mailer_usermailer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mailer_usermailerlogentry`
--

LOCK TABLES `mailer_usermailerlogentry` WRITE;
/*!40000 ALTER TABLE `mailer_usermailerlogentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `mailer_usermailerlogentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mayan_statistics_statisticresult`
--

DROP TABLE IF EXISTS `mayan_statistics_statisticresult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mayan_statistics_statisticresult` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(50) NOT NULL,
  `datetime` datetime(6) NOT NULL,
  `serialize_data` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mayan_statistics_statisticresult_2dbcba41` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mayan_statistics_statisticresult`
--

LOCK TABLES `mayan_statistics_statisticresult` WRITE;
/*!40000 ALTER TABLE `mayan_statistics_statisticresult` DISABLE KEYS */;
/*!40000 ALTER TABLE `mayan_statistics_statisticresult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata_documentmetadata`
--

DROP TABLE IF EXISTS `metadata_documentmetadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata_documentmetadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  `document_id` int(11) NOT NULL,
  `metadata_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `metadata_documentmetadata_document_id_34933d91_uniq` (`document_id`,`metadata_type_id`),
  KEY `metadata_documentmetadata_2063c160` (`value`),
  KEY `metadata_d_metadata_type_id_b3eee3ed_fk_metadata_metadatatype_id` (`metadata_type_id`),
  CONSTRAINT `metadata_d_metadata_type_id_b3eee3ed_fk_metadata_metadatatype_id` FOREIGN KEY (`metadata_type_id`) REFERENCES `metadata_metadatatype` (`id`),
  CONSTRAINT `metadata_documentm_document_id_b0b10479_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata_documentmetadata`
--

LOCK TABLES `metadata_documentmetadata` WRITE;
/*!40000 ALTER TABLE `metadata_documentmetadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `metadata_documentmetadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata_documenttypemetadatatype`
--

DROP TABLE IF EXISTS `metadata_documenttypemetadatatype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata_documenttypemetadatatype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `required` tinyint(1) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `metadata_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `metadata_documenttypemetadatatype_document_type_id_c88d1cb7_uniq` (`document_type_id`,`metadata_type_id`),
  KEY `metadata_d_metadata_type_id_3590987f_fk_metadata_metadatatype_id` (`metadata_type_id`),
  CONSTRAINT `metadata__document_type_id_3cd67d88_fk_documents_documenttype_id` FOREIGN KEY (`document_type_id`) REFERENCES `documents_documenttype` (`id`),
  CONSTRAINT `metadata_d_metadata_type_id_3590987f_fk_metadata_metadatatype_id` FOREIGN KEY (`metadata_type_id`) REFERENCES `metadata_metadatatype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata_documenttypemetadatatype`
--

LOCK TABLES `metadata_documenttypemetadatatype` WRITE;
/*!40000 ALTER TABLE `metadata_documenttypemetadatatype` DISABLE KEYS */;
/*!40000 ALTER TABLE `metadata_documenttypemetadatatype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata_metadatatype`
--

DROP TABLE IF EXISTS `metadata_metadatatype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata_metadatatype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) NOT NULL,
  `label` varchar(48) NOT NULL,
  `default` varchar(128) DEFAULT NULL,
  `lookup` longtext,
  `validation` varchar(64) NOT NULL,
  `parser` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata_metadatatype`
--

LOCK TABLES `metadata_metadatatype` WRITE;
/*!40000 ALTER TABLE `metadata_metadatatype` DISABLE KEYS */;
/*!40000 ALTER TABLE `metadata_metadatatype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motd_message`
--

DROP TABLE IF EXISTS `motd_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motd_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(32) NOT NULL,
  `message` longtext NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `start_datetime` datetime(6) DEFAULT NULL,
  `end_datetime` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motd_message`
--

LOCK TABLES `motd_message` WRITE;
/*!40000 ALTER TABLE `motd_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `motd_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocr_documentpageocrcontent`
--

DROP TABLE IF EXISTS `ocr_documentpageocrcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocr_documentpageocrcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext NOT NULL,
  `document_page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document_page_id` (`document_page_id`),
  CONSTRAINT `ocr_docum_document_page_id_3b584311_fk_documents_documentpage_id` FOREIGN KEY (`document_page_id`) REFERENCES `documents_documentpage` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocr_documentpageocrcontent`
--

LOCK TABLES `ocr_documentpageocrcontent` WRITE;
/*!40000 ALTER TABLE `ocr_documentpageocrcontent` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocr_documentpageocrcontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocr_documenttypesettings`
--

DROP TABLE IF EXISTS `ocr_documenttypesettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocr_documenttypesettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auto_ocr` tinyint(1) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document_type_id` (`document_type_id`),
  CONSTRAINT `ocr_docum_document_type_id_33ca6c9c_fk_documents_documenttype_id` FOREIGN KEY (`document_type_id`) REFERENCES `documents_documenttype` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocr_documenttypesettings`
--

LOCK TABLES `ocr_documenttypesettings` WRITE;
/*!40000 ALTER TABLE `ocr_documenttypesettings` DISABLE KEYS */;
INSERT INTO `ocr_documenttypesettings` VALUES (1,1,1);
/*!40000 ALTER TABLE `ocr_documenttypesettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocr_documentversionocrerror`
--

DROP TABLE IF EXISTS `ocr_documentversionocrerror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocr_documentversionocrerror` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime_submitted` datetime(6) NOT NULL,
  `result` longtext,
  `document_version_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ocr_documentversionocrerror_98960ed2` (`datetime_submitted`),
  KEY `ocr_document_version_id_153016f5_fk_documents_documentversion_id` (`document_version_id`),
  CONSTRAINT `ocr_document_version_id_153016f5_fk_documents_documentversion_id` FOREIGN KEY (`document_version_id`) REFERENCES `documents_documentversion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocr_documentversionocrerror`
--

LOCK TABLES `ocr_documentversionocrerror` WRITE;
/*!40000 ALTER TABLE `ocr_documentversionocrerror` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocr_documentversionocrerror` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_role`
--

DROP TABLE IF EXISTS `permissions_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_role`
--

LOCK TABLES `permissions_role` WRITE;
/*!40000 ALTER TABLE `permissions_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_role_groups`
--

DROP TABLE IF EXISTS `permissions_role_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_role_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_role_groups_role_id_e958b421_uniq` (`role_id`,`group_id`),
  KEY `permissions_role_groups_group_id_cc37ac20_fk_auth_group_id` (`group_id`),
  CONSTRAINT `permissions_role_groups_group_id_cc37ac20_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permissions_role_groups_role_id_bd491961_fk_permissions_role_id` FOREIGN KEY (`role_id`) REFERENCES `permissions_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_role_groups`
--

LOCK TABLES `permissions_role_groups` WRITE;
/*!40000 ALTER TABLE `permissions_role_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions_role_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_role_permissions`
--

DROP TABLE IF EXISTS `permissions_role_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_role_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `storedpermission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_role_permissions_role_id_d9d895c7_uniq` (`role_id`,`storedpermission_id`),
  KEY `storedpermission_id_8f157086_fk_permissions_storedpermission_id` (`storedpermission_id`),
  CONSTRAINT `permissions_role_permiss_role_id_9313389c_fk_permissions_role_id` FOREIGN KEY (`role_id`) REFERENCES `permissions_role` (`id`),
  CONSTRAINT `storedpermission_id_8f157086_fk_permissions_storedpermission_id` FOREIGN KEY (`storedpermission_id`) REFERENCES `permissions_storedpermission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_role_permissions`
--

LOCK TABLES `permissions_role_permissions` WRITE;
/*!40000 ALTER TABLE `permissions_role_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_storedpermission`
--

DROP TABLE IF EXISTS `permissions_storedpermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_storedpermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_storedpermission_namespace_fb9a3732_uniq` (`namespace`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_storedpermission`
--

LOCK TABLES `permissions_storedpermission` WRITE;
/*!40000 ALTER TABLE `permissions_storedpermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions_storedpermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_emailbasemodel`
--

DROP TABLE IF EXISTS `sources_emailbasemodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_emailbasemodel` (
  `intervalbasemodel_ptr_id` int(11) NOT NULL,
  `host` varchar(128) NOT NULL,
  `ssl` tinyint(1) NOT NULL,
  `port` int(10) unsigned DEFAULT NULL,
  `username` varchar(96) NOT NULL,
  `password` varchar(96) NOT NULL,
  `metadata_attachment_name` varchar(128) NOT NULL,
  `from_metadata_type_id` int(11),
  `subject_metadata_type_id` int(11),
  `store_body` tinyint(1) NOT NULL,
  PRIMARY KEY (`intervalbasemodel_ptr_id`),
  KEY `sourc_from_metadata_type_id_6f5ea19d_fk_metadata_metadatatype_id` (`from_metadata_type_id`),
  KEY `so_subject_metadata_type_id_d36e6373_fk_metadata_metadatatype_id` (`subject_metadata_type_id`),
  CONSTRAINT `D92104c599279888ffb4d6a555689b42` FOREIGN KEY (`intervalbasemodel_ptr_id`) REFERENCES `sources_intervalbasemodel` (`outofprocesssource_ptr_id`),
  CONSTRAINT `so_subject_metadata_type_id_d36e6373_fk_metadata_metadatatype_id` FOREIGN KEY (`subject_metadata_type_id`) REFERENCES `metadata_metadatatype` (`id`),
  CONSTRAINT `sourc_from_metadata_type_id_6f5ea19d_fk_metadata_metadatatype_id` FOREIGN KEY (`from_metadata_type_id`) REFERENCES `metadata_metadatatype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_emailbasemodel`
--

LOCK TABLES `sources_emailbasemodel` WRITE;
/*!40000 ALTER TABLE `sources_emailbasemodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_emailbasemodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_imapemail`
--

DROP TABLE IF EXISTS `sources_imapemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_imapemail` (
  `emailbasemodel_ptr_id` int(11) NOT NULL,
  `mailbox` varchar(64) NOT NULL,
  PRIMARY KEY (`emailbasemodel_ptr_id`),
  CONSTRAINT `D658cb5631c596d66e6d3c0487a26156` FOREIGN KEY (`emailbasemodel_ptr_id`) REFERENCES `sources_emailbasemodel` (`intervalbasemodel_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_imapemail`
--

LOCK TABLES `sources_imapemail` WRITE;
/*!40000 ALTER TABLE `sources_imapemail` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_imapemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_interactivesource`
--

DROP TABLE IF EXISTS `sources_interactivesource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_interactivesource` (
  `source_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`source_ptr_id`),
  CONSTRAINT `sources_interactives_source_ptr_id_c31b33f5_fk_sources_source_id` FOREIGN KEY (`source_ptr_id`) REFERENCES `sources_source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_interactivesource`
--

LOCK TABLES `sources_interactivesource` WRITE;
/*!40000 ALTER TABLE `sources_interactivesource` DISABLE KEYS */;
INSERT INTO `sources_interactivesource` VALUES (1);
/*!40000 ALTER TABLE `sources_interactivesource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_intervalbasemodel`
--

DROP TABLE IF EXISTS `sources_intervalbasemodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_intervalbasemodel` (
  `outofprocesssource_ptr_id` int(11) NOT NULL,
  `interval` int(10) unsigned NOT NULL,
  `uncompress` varchar(1) NOT NULL,
  `document_type_id` int(11) NOT NULL,
  PRIMARY KEY (`outofprocesssource_ptr_id`),
  KEY `sources_i_document_type_id_90c75bb5_fk_documents_documenttype_id` (`document_type_id`),
  CONSTRAINT `e4fcd88c1896ada750c5046d21f11b59` FOREIGN KEY (`outofprocesssource_ptr_id`) REFERENCES `sources_outofprocesssource` (`source_ptr_id`),
  CONSTRAINT `sources_i_document_type_id_90c75bb5_fk_documents_documenttype_id` FOREIGN KEY (`document_type_id`) REFERENCES `documents_documenttype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_intervalbasemodel`
--

LOCK TABLES `sources_intervalbasemodel` WRITE;
/*!40000 ALTER TABLE `sources_intervalbasemodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_intervalbasemodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_outofprocesssource`
--

DROP TABLE IF EXISTS `sources_outofprocesssource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_outofprocesssource` (
  `source_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`source_ptr_id`),
  CONSTRAINT `sources_outofprocess_source_ptr_id_f911cabb_fk_sources_source_id` FOREIGN KEY (`source_ptr_id`) REFERENCES `sources_source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_outofprocesssource`
--

LOCK TABLES `sources_outofprocesssource` WRITE;
/*!40000 ALTER TABLE `sources_outofprocesssource` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_outofprocesssource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_pop3email`
--

DROP TABLE IF EXISTS `sources_pop3email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_pop3email` (
  `emailbasemodel_ptr_id` int(11) NOT NULL,
  `timeout` int(10) unsigned NOT NULL,
  PRIMARY KEY (`emailbasemodel_ptr_id`),
  CONSTRAINT `a4f05c245f6b5107c3586d8dca7da745` FOREIGN KEY (`emailbasemodel_ptr_id`) REFERENCES `sources_emailbasemodel` (`intervalbasemodel_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_pop3email`
--

LOCK TABLES `sources_pop3email` WRITE;
/*!40000 ALTER TABLE `sources_pop3email` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_pop3email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_sanescanner`
--

DROP TABLE IF EXISTS `sources_sanescanner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_sanescanner` (
  `interactivesource_ptr_id` int(11) NOT NULL,
  `device_name` varchar(255) NOT NULL,
  `mode` varchar(16) NOT NULL,
  `resolution` int(10) unsigned DEFAULT NULL,
  `adf_mode` varchar(16) NOT NULL,
  `source` varchar(32) NOT NULL,
  PRIMARY KEY (`interactivesource_ptr_id`),
  CONSTRAINT `aec5a95f8a2b68db84d2e52edf476f3a` FOREIGN KEY (`interactivesource_ptr_id`) REFERENCES `sources_interactivesource` (`source_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_sanescanner`
--

LOCK TABLES `sources_sanescanner` WRITE;
/*!40000 ALTER TABLE `sources_sanescanner` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_sanescanner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_source`
--

DROP TABLE IF EXISTS `sources_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_source`
--

LOCK TABLES `sources_source` WRITE;
/*!40000 ALTER TABLE `sources_source` DISABLE KEYS */;
INSERT INTO `sources_source` VALUES (1,'Default',1);
/*!40000 ALTER TABLE `sources_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_sourcelog`
--

DROP TABLE IF EXISTS `sources_sourcelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_sourcelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime(6) NOT NULL,
  `message` longtext NOT NULL,
  `source_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sources_sourcelog_source_id_c681ef4b_fk_sources_source_id` (`source_id`),
  CONSTRAINT `sources_sourcelog_source_id_c681ef4b_fk_sources_source_id` FOREIGN KEY (`source_id`) REFERENCES `sources_source` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_sourcelog`
--

LOCK TABLES `sources_sourcelog` WRITE;
/*!40000 ALTER TABLE `sources_sourcelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_sourcelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_stagingfoldersource`
--

DROP TABLE IF EXISTS `sources_stagingfoldersource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_stagingfoldersource` (
  `interactivesource_ptr_id` int(11) NOT NULL,
  `folder_path` varchar(255) NOT NULL,
  `preview_width` int(11) NOT NULL,
  `preview_height` int(11) DEFAULT NULL,
  `uncompress` varchar(1) NOT NULL,
  `delete_after_upload` tinyint(1) NOT NULL,
  PRIMARY KEY (`interactivesource_ptr_id`),
  CONSTRAINT `dfb56eceafd47b8689f29acd12b6982c` FOREIGN KEY (`interactivesource_ptr_id`) REFERENCES `sources_interactivesource` (`source_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_stagingfoldersource`
--

LOCK TABLES `sources_stagingfoldersource` WRITE;
/*!40000 ALTER TABLE `sources_stagingfoldersource` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_stagingfoldersource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_watchfoldersource`
--

DROP TABLE IF EXISTS `sources_watchfoldersource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_watchfoldersource` (
  `intervalbasemodel_ptr_id` int(11) NOT NULL,
  `folder_path` varchar(255) NOT NULL,
  PRIMARY KEY (`intervalbasemodel_ptr_id`),
  CONSTRAINT `D007a4e6a2a2bd08332a87844c853363` FOREIGN KEY (`intervalbasemodel_ptr_id`) REFERENCES `sources_intervalbasemodel` (`outofprocesssource_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_watchfoldersource`
--

LOCK TABLES `sources_watchfoldersource` WRITE;
/*!40000 ALTER TABLE `sources_watchfoldersource` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources_watchfoldersource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources_webformsource`
--

DROP TABLE IF EXISTS `sources_webformsource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources_webformsource` (
  `interactivesource_ptr_id` int(11) NOT NULL,
  `uncompress` varchar(1) NOT NULL,
  PRIMARY KEY (`interactivesource_ptr_id`),
  CONSTRAINT `d808fa27fb52fe5a66527496f64e8eb6` FOREIGN KEY (`interactivesource_ptr_id`) REFERENCES `sources_interactivesource` (`source_ptr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources_webformsource`
--

LOCK TABLES `sources_webformsource` WRITE;
/*!40000 ALTER TABLE `sources_webformsource` DISABLE KEYS */;
INSERT INTO `sources_webformsource` VALUES (1,'a');
/*!40000 ALTER TABLE `sources_webformsource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_tag`
--

DROP TABLE IF EXISTS `tags_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(128) NOT NULL,
  `color` varchar(7) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_tag`
--

LOCK TABLES `tags_tag` WRITE;
/*!40000 ALTER TABLE `tags_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_tag_documents`
--

DROP TABLE IF EXISTS `tags_tag_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_tag_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_tag_documents_tag_id_7698ad5e_uniq` (`tag_id`,`document_id`),
  KEY `tags_tag_documents_document_id_cbf643eb_fk_documents_document_id` (`document_id`),
  CONSTRAINT `tags_tag_documents_document_id_cbf643eb_fk_documents_document_id` FOREIGN KEY (`document_id`) REFERENCES `documents_document` (`id`),
  CONSTRAINT `tags_tag_documents_tag_id_2fe342c7_fk_tags_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_tag_documents`
--

LOCK TABLES `tags_tag_documents` WRITE;
/*!40000 ALTER TABLE `tags_tag_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_tag_documents` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-22 11:19:30
